﻿using System.Data.Entity;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Backpack.Container;
using Backpack.Models;
using Backpack.Models.Process;

namespace Backpack
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
              
            Database.SetInitializer(new DbInitializer());
            Working.Start();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            IocContainer.Setup();
        }
        
        protected void Application_End()
        {
            IocContainer.Dispose();
        }
    }
}