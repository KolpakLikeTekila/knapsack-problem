﻿using System.Collections.Generic;
using System.Data.Entity;
using Backpack.DomainServices;
using Backpack.Entity;

namespace Backpack
{
    public class DbInitializer : DropCreateDatabaseAlways<DBContext>
    {
        protected override void Seed(DBContext context)
        {
            var things = new List<Thing>
            {
                new Thing
                {
                    Name = "вещь1",
                    Cost = 5,
                    Weight = 1
                },
                new Thing
                {
                    Name = "вещь2",
                    Cost = 1,
                    Weight = 8
                },
                new Thing
                {
                    Name = "вещь3",
                    Cost = 8,
                    Weight = 6
                },
                new Thing
                {
                    Name = "вещь4",
                    Cost = 15,
                    Weight = 20
                },
                new Thing
                {
                    Name = "вещь5",
                    Cost = 11,
                    Weight = 2
                },
                new Thing
                {
                    Name = "вещь6",
                    Cost = 2,
                    Weight = 1
                }
            };
            var th1 = new Thing
            {
                Name = "вещь8",
                Cost = 10,
                Weight = 5
            };

            var rucksack1 = new Rucksack {Id = 1, Capacity = 15, Things = things};
            var rucksack2 = new Rucksack {Id = 2, Capacity = 5, Things = new List<Thing> {th1}};

            var task1 = new TaskRusksack { Name = 1, Rucksack = rucksack1};
            var task2 = new TaskRusksack {Name = 2, Rucksack = rucksack2};


            context.Set<Rucksack>().AddRange(new List<Rucksack> {rucksack1, rucksack2});
            things.ForEach(m => context.Set<Thing>().Add(m));
            context.Set<TaskRusksack>().AddRange(new List<TaskRusksack> {task1, task2});

            base.Seed(context);
        }
    }
}