﻿using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Backpack.DomainServices;
using Backpack.DomainServices.Interfaces;
using Backpack.Entity;
using Backpack.ViewModels;
using Backpack.ViewModels.Interfaces;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Backpack.Castle
{
    public class ApplicationCastleInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            // регистрируем каждый контроллер по отдельности
            var controllers = Assembly.GetExecutingAssembly()
                .GetTypes().Where(x => x.BaseType == typeof(Controller)).ToList();
            foreach (var controller in controllers)
            {
                container.Register(Component.For(controller).LifestylePerWebRequest());
            }
            
            container.Register(Component.For<IRucksackViewModel>().ImplementedBy<RucksackViewModel>());
            container.Register(Component.For<IRucksackDomainService>().ImplementedBy<RucksackDomainService>());
            container.Register(Component.For<IThingDomainService>().ImplementedBy<ThingDomainService>());
        }
    }
}