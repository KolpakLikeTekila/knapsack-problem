﻿using System.Collections.Generic;
using Backpack.Dto;

namespace Backpack.ViewModels.Interfaces
{
    public interface IRucksackViewModel
    {
        List<TaskListDto> List();

        TaskDto Detail(long id);
    }
}