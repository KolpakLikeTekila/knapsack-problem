﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backpack.DomainServices;
using Backpack.DomainServices.Interfaces;
using Backpack.Dto;
using Backpack.Entity;
using Backpack.ViewModels.Interfaces;

namespace Backpack.ViewModels
{
    public class RucksackViewModel : IRucksackViewModel
    {
        public IRucksackDomainService RucksackDomainService { get; set; }
        
        public IThingDomainService ThingDomainService { get; set; }

        public List<TaskListDto> List()
        {
            var thingIds = new List<long>();
            var things = new List<TaskListDto>();
            thingIds = RucksackDomainService.GetAll().Select(rucksack => rucksack.Id).ToList();
            if (!thingIds.Any())
                return new List<TaskListDto>();
            ILookup<long, Thing> thingLookup;
            thingLookup = ThingDomainService.GetAll().ToList().ToLookup(thing => thing.RucksackId);

            things = RucksackDomainService.GetAll().Select(rucksack => new
            {
                rucksack.Id,
                rucksack.Capacity,
                Name = rucksack.TaskRusksack.Name.ToString(),
                rucksack.TaskRusksack.Process,
                rucksack.TaskRusksack.Status,
            }).ToList().Select(rucksack => new TaskListDto
            {
                Id = rucksack.Id,
                Capacity = rucksack.Capacity,
                Name = rucksack.Name,
                Status = rucksack.Status,
                Percent = GetPercent(rucksack.Process, thingLookup[rucksack.Id].Count()),
                Cost = thingLookup[rucksack.Id].Where(thing => thing.Take).Sum(thing => thing.Cost)
            }).ToList();

            return things;
        }

        private string GetPercent(long countIter, int countLvel)
        {
            var maxiter = Factorial(countLvel);
            var result = (countIter * 100 / maxiter).ToString();
            return result + "%";
        }

        private long Factorial(long n)
        {
            if (n <= 1) return 1;
            return n * Factorial(n - 1) + 1;
        }

        public TaskDto Detail(long id)
        {
            Rucksack rucksack;
            var things = new List<ThingDto>();
            var thingSolutions = new List<ThingDto>();

            rucksack = RucksackDomainService.Get(id);
            if (rucksack == null)
                return null;
            var thingsQueryable = ThingDomainService.GetAll().Where(thing => thing.RucksackId == rucksack.Id);
            things = thingsQueryable.Select(thing => new ThingDto
            {
                Cost = thing.Cost,
                Name = thing.Name,
                Weight = thing.Weight
            }).ToList();

            thingSolutions = thingsQueryable.Where(thing => thing.Take).Select(thing => new ThingDto
            {
                Cost = thing.Cost,
                Name = thing.Name,
                Weight = thing.Weight
            }).ToList();

            var task = new TaskDto
            {
                Capacity = rucksack.Capacity,
                Name = rucksack.TaskRusksack.Name.ToString(),
                Status = rucksack.TaskRusksack.Status,
                Time = TimeSpan.FromMilliseconds(rucksack.TaskRusksack.Time).ToString(),
                Cost = thingSolutions.Sum(thing => thing.Cost),
                Things = things,
                ThingSolutions = thingSolutions
            };
            return task;
        }
    }
}