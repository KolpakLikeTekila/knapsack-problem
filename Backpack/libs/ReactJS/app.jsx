﻿var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var IndexRoute = ReactRouter.IndexRoute;
var browserHistory = ReactRouter.browserHistory;
var Link = ReactRouter.Link;

ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/">
            <Route path="newrucksack" component={CreateRucksack}/>
            <Route path="details/:id" component={Info}/>
            <IndexRoute component={List}/>
        </Route>
        <Route path="*" component={NotFound}/>
    </Router>,
    document.getElementById("react")
);