﻿class TaskDetail extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var { Capacity, Status, Things } = this.props;
        let stat = null;
        if (Status) {
            stat = <div className="col-md-6">
                       <label>Выполнено</label>
                   </div>;
        } else {
            stat = <div className="col-md-6">
                       <label>Выполняется</label>
                   </div>;
        }
        return (
            <div id="rucksackInfo">
                <div className="panel-body">
                    <h2 className="h2Deatail">
                        Детали задачи
                    </h2>
                    <div className="row">
                        <label className="control-label col-md-6">ВМЕСТИМОСТЬ РЮКЗАКА</label>
                        <label className="control-label col-md-6">СТАТУС</label>
                    </div>
                    <div className="form-group row">
                        <div className="col-md-6">{Capacity}</div>
                        {stat}
                    </div>
                    <div className="row">
                        <label className="control-label col-md-6">Предмет</label>
                        <label className="control-label col-md-3">Ценность</label>
                        <label className="control-label col-md-2">Вес</label>
                    </div>
                </div>
                {Things.map(thing =>
                    <div id="itemTask" className="panel-body">
                        <div className="task col-md-6">{thing.Name}</div>
                        <div className="task col-md-3">{thing.Cost}</div>
                        <div className="task col-md-2">{thing.Weight}</div>
                    </div>
                )}
            </div>
        );
    }
}