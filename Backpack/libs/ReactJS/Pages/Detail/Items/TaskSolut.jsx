﻿class TaskSolut extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        var { Cost, Time, Things } = this.props;
        return (
            <div id="taskSolut">
                <div className="panel-body">
                    <h2 className="h2Deatail">
                        Решение
                    </h2>
                    <div className="row">
                        <label className="control-label col-md-6">МАКСИМАЛЬНАЯ ЦЕННОСТЬ</label>
                        <label className="control-label col-md-6">ВРЕМЯ ВЫПОЛНЕНИЯ</label>
                    </div>
                    <div className="form-group row">
                        <div className="col-md-6">{Cost}</div>
                        <div className="col-md-6">{Time}</div>
                    </div>
                    <div className="row">
                        <label className="control-label col-md-6">Предмет</label>
                        <label className="control-label col-md-3">Ценность</label>
                        <label className="control-label col-md-2">Вес</label>
                    </div>
                </div>
                {Things.map(thing =>
                    <div id="itemTask" className="panel-body">
                        <div className="task col-md-6">{thing.Name}</div>
                        <div className="task col-md-3">{thing.Cost}</div>
                        <div className="task col-md-2">{thing.Weight}</div>
                    </div>
                )}
            </div>
        );
    }
}