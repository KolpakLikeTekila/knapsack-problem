﻿class Info extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Name: null,
            Capacity: null,
            Status: null,
            Things: [],
            ThingSolutions: [],
            Time: null,
            Cost: null,
        };
    }

    componentDidMount() {
        this.ajaxGetDate();
    }

    ajaxGetDate() {
        $.getJSON("/detail",
            { id: this.props.params.id },
            function(data) {
                this.setState({
                    Name: data.Name,
                    Capacity: data.Capacity,
                    Status: data.Status,
                    Things: data.Things,
                    ThingSolutions: data.ThingSolutions,
                    Time: data.Time,
                    Cost: data.Cost,
                });
            }.bind(this));
    }

    render() {
        var { Name, Capacity, Status, Things, ThingSolutions, Time, Cost } = this.state;
        return (
            <div>
                <div className="panel-primary">
                    <div className="panel-heading">
                        <Link to="/">←</Link>
                        Задача №{Name}
                    </div>
                </div>
                <div className="row row-flex">
                    <div className="col-md-6">
                        <TaskDetail Capacity={Capacity} Status={Status} Things={Things}/>
                    </div>
                    <div className="col-md-6">
                        <TaskSolut Things={ThingSolutions} Time={Time} Cost={Cost}/>
                    </div>
                </div>
            </div>
        );
    }
}