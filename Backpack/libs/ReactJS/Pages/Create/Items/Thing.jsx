﻿class Thing extends React.Component {
    constructor(props) {
        super(props);
        this.refs.Name = this.props.Name;
        this.refs.Cost = this.props.Cost;
        this.refs.Weight = this.props.Weight;
    }

    removeThing() {
        this.props.onRemove(this.props.NumberThing);
    }

    updateThing() {
        this.props.onUpdate(this.props.NumberThing, this.refs.Name.value, this.refs.Cost.value, this.refs.Weight.value);
    }

    render() {
        return (
            <div id="itemThing" className="text-center row itemThing">
                <div className="col-md-7">
                    <input type="text" className="form-control" ref="Name" onChange={this.updateThing.bind(this) }/>
                </div>
                <div className="col-md-2">
                    <input type="number" className="form-control" ref="Cost" onChange={this.updateThing.bind(this) }/>
                </div>
                <div className="col-md-2">
                    <input type="number" className="form-control" ref="Weight" onChange={this.updateThing.bind(this)
}/>
                </div>
                <button onClick={this.removeThing.bind(this)} className="btn btn-default">
                    X
                </button>
            </div>
        );
    }
}