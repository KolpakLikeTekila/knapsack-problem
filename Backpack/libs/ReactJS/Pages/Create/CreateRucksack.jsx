﻿class CreateRucksack extends React.Component {

    constructor(props) {
        super(props);
        var countThings = 1;

        this.state = {
            Capacity: "",
            CountThings: countThings,
            Things: [{NumberThing: countThings, Name: "", Cost: "", Weight: ""}],
        };
    }

    addThing() {
        var countThings = this.state.CountThings;
        countThings++;
        var thing = {NumberThing: countThings, Name: "", Cost: "", Weight: ""};
        var data = this.state.Things;
        data.push(thing);
        this.setState({Things: data, CountThings: countThings});
    }

    onCapacity(e) {
        this.setState({Capacity: e.target.value});
    }

    getNumberThing(value, arr) {
        var element = -1;
        arr.forEach(function (item, i) {
            if (item["NumberThing"] === value) {
                element = i;
            }
        });
        return element;
    }

    onRemoveThing(thing) {
        var data = this.state.Things;
        var index = this.getNumberThing(thing, data);
        if (index > -1) {
            data.splice(index, 1);
            this.setState({things: data});
        }
    }

    onCreateTask() {
        var things = this.state.Things.map(a => {
            var obj = {};
            obj.Name = a.Name;
            obj.Cost = a.Cost;
            obj.Weight = a.Weight;
            return obj;
        });
        var params = {
            "capacity": this.state.Capacity,
            "things": things
        };
        $.post("create", params);
        this.onCansel();
    }

    onCansel() {
        browserHistory.push("/");
    }

    onUpdateThing(numberThing, name, cost, weight) {
        var data = this.state.Things;
        var index = this.getNumberThing(numberThing, data);
        if (index > -1) {
            var thing = {NumberThing: numberThing, Name: name, Cost: cost, Weight: weight};
            data.splice(index, 1, thing);
            this.setState({things: data});
        }
    }

    render() {
        return (
            <div className="panel-primary">
                <div className="panel-heading header">
                    <Link to="/">←</Link>
                    Новая задача
                </div>
                <div className="panel-body col-md-12">
                    <div className="col-md-12">
                        <lable className="control-label col-md-10 header">ВМЕСТИМОСТЬ РЮКЗАКА</lable>
                        <input type="number" className="form-control col-md-3" value={this.state.Capacity}
                               onChange={this.onCapacity.bind(this)}/>
                    </div>
                </div>
                <div className="panel-body">
                    <div className="row">
                        <label className="control-label col-md-7">Предмет</label>
                        <label className="control-label col-md-2">Ценность</label>
                        <label className="control-label col-md-2">Вес</label>
                    </div>
                    <div id="newThing">
                        {this.state.Things.map(thing =>
                            <Thing key={thing.NumberThing}
                                   NumberThing={thing.NumberThing}
                                   Name={thing.Name}
                                   Cost={thing.Cost}
                                   Weight={thing.Weight } onRemove={this.onRemoveThing.bind(this)}
                                   onUpdate={this.onUpdateThing.bind(this)}/>
                        )}
                    </div>
                    <div className="panel-body col-md-10" style={{margin: 15 + "px"}}>
                        <button onClick={this.addThing.bind(this)} className="btn btn-default">
                            +
                        </button>
                    </div>
                </div>
                <div className="panel-footer input-group">
                    <div className="center-block">
                        <button onClick={this.onCreateTask.bind(this)} className="btn btn-default btncontrol">
                            СОЗДАТЬ
                        </button>
                        <button onClick={this.onCansel} className="btn btn-default btncontrol">
                            ОТМЕНИТЬ
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}