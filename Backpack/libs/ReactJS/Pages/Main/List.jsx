﻿class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: []
        };
    }

    componentDidMount() {
        this.timerId = setInterval(
            () => this.ajaxGetDate(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerId);
    }

    ajaxGetDate() {
        $.getJSON("/list",
            function (data) {
                this.setState({tasks: data});
            }.bind(this));
    }

    goToCreateRucksack() {
        browserHistory.push("/newrucksack");
    }

    render() {
        return (
            <div className="panel-primary">
                <div className="panel-heading header">
                    Список задач
                    <button onClick={this.goToCreateRucksack} className="btn btn-default btncontrol right btnNewTask">
                        НОВАЯ ЗАДАЧА
                    </button>
                </div>
                <div className="panel-body headerTask">
                    <label className="control-label col-md-3">№ ЗАДАЧИ</label>
                    <label className="control-label col-md-2">ВМЕСТИМОСТЬ РЮКЗАКА</label>
                    <label className="control-label col-md-2">МАКСИМАЛЬНАЯ ЦЕННОСТЬ</label>
                    <label className="control-label col-md-2">ПРОЦЕНТ ВЫПОЛНЕНИЯ</label>
                    <label className="control-label col-md-2">СТАТУС</label>
                </div>
                <div className="form-group">
                    <div id="Tasks">
                        {this.state.tasks.map(task => <Task Id={task.Id}
                                                            Name={task.Name}
                                                            Capacity={task.Capacity}
                                                            Cost={task.Cost}
                                                            Percent={task.Percent}
                                                            Status={task.Status}/>)}
                    </div>
                </div>
            </div>
        );
    }
}