﻿class Task extends React.Component {
    constructor(props) {
        super(props);
    }

    removeTask() {
        $.post("remove", { id: this.props.Id });
    }

    render() {
        var { Name, Capacity, Cost, Percent, Status, Id } = this.props;
        let complited = null;
        if (Status) {
            let path = /details/ + Id;
            complited =
                <div>
                    <div className="task col-md-2">{Cost}</div>
                    <div className="task col-md-2 col-md-offset-2">
                        <Link to={path}>Выполнено</Link>
                    </div>
                </div>;
        } else {
            complited =
                <div>
                    <div className="task col-md-2 col-md-offset-2">{Percent}</div>
                    <div className="task col-md-2">
                        <label>Выполняется</label>
                    </div>
                </div>;
        }
        return (
            <div id="itemTask" className="panel-body" styleName="margin-bottom:15px">
                <div className="task col-md-3">{Name}</div>
                <div className="task col-md-2">{Capacity}</div>
                {complited}
                <button onClick={this.removeTask.bind(this)} className="btn btn-default">
                    X
                </button>
            </div>
        );
    }
}