"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var app_routing_1 = require("./app.routing");
var app_component_1 = require("./app.component");
var not_found_component_1 = require("./Pages/Components/not-found.component");
var create_component_1 = require("./Pages/Components/Create/create.component");
var things_component_1 = require("./Pages/Components/Create/things.component");
var thing_component_1 = require("./Pages/Components/Create/thing.component");
var main_component_1 = require("./Pages/Components/Main/main.component");
var tasks_component_1 = require("./Pages/Components/Main/tasks.component");
var task_component_1 = require("./Pages/Components/Main/task.component");
var common_1 = require("@angular/common");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.ReactiveFormsModule, http_1.HttpModule, app_routing_1.routing, common_1.CommonModule],
        declarations: [app_component_1.AppComponent, main_component_1.MainComponent, create_component_1.CreateComponent, not_found_component_1.NotFoundComponent, things_component_1.ThingsComponent, thing_component_1.ThingComponent, tasks_component_1.TasksComponent, task_component_1.TaskComponent],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map