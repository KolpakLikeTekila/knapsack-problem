"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var not_found_component_1 = require("./Pages/Components/not-found.component");
var create_component_1 = require("./Pages/Components/Create/create.component");
var main_component_1 = require("./Pages/Components/Main/main.component");
var appRoutes = [
    { path: '', redirectTo: '/main', pathMatch: 'full' },
    { path: 'main', component: main_component_1.MainComponent },
    { path: 'create', component: create_component_1.CreateComponent },
    { path: '**', component: not_found_component_1.NotFoundComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map