import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NotFoundComponent} from "./Pages/Components/not-found.component"; 
import {CreateComponent} from "./Pages/Components/Create/create.component";
import {MainComponent} from "./Pages/Components/Main/main.component";

const appRoutes: Routes = [
    {path: '', redirectTo: '/main', pathMatch:'full'},
    {path: 'main', component: MainComponent},
    {path: 'create', component: CreateComponent},
    { path: '**', component: NotFoundComponent }
];

export const routing: ModuleWithProviders =
    RouterModule.forRoot(appRoutes);