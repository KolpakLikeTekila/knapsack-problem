"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
require("rxjs/add/operator/do");
require("rxjs/add/operator/catch");
var router_1 = require("@angular/router");
var global_1 = require("../../../Shared/global");
var CreateComponent = (function () {
    function CreateComponent(formBuilder, http, router) {
        this.formBuilder = formBuilder;
        this.http = http;
        this.router = router;
    }
    CreateComponent.prototype.goMain = function () {
        this.router.navigate(['']);
    };
    CreateComponent.prototype.ngOnInit = function () {
        this.formRucksack = this.formBuilder.group({
            capacity: [0],
            things: this.formBuilder.array([])
        });
    };
    CreateComponent.prototype.post = function (url, model) {
        var body = JSON.stringify(model);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(url, body, options);
    };
    CreateComponent.prototype.createRucksack = function (formData) {
        this.post("create", formData._value);
        this.goMain();
    };
    CreateComponent.prototype.cansel = function () {
        this.goMain();
    };
    return CreateComponent;
}());
CreateComponent = __decorate([
    core_1.Component({
        templateUrl: global_1.Global.pathToView('create', 'create')
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder, http_1.Http, router_1.Router])
], CreateComponent);
exports.CreateComponent = CreateComponent;
//# sourceMappingURL=create.component.js.map