﻿import {ThingComponent} from "./thing.component";
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {Component, Input, OnInit} from "@angular/core"; 
import {Global} from "../../../Shared/global";

@Component({
    selector: 'things',
    templateUrl: Global.pathToView('things', 'create')
})

export class ThingsComponent implements OnInit {

    @Input() formThings: FormGroup;
    private things: Array<ThingComponent> = [];

    constructor(private formBuilder: FormBuilder, ) {
    }

    ngOnInit() {
        var a = this.initThingGroup();
        this.formThings.addControl('things', this.formBuilder.array([this.initThingGroup()]));
    }

    private initThingGroup(): FormGroup {
        var thingGroup = this.formBuilder.group({
            Weight: [0],
            Name: [''],
            Cost: [0]
        });
        return thingGroup;
    }

    addThing() {
        var things = <FormArray>this.formThings.controls['things'];
        things.push(this.initThingGroup());
    }

    removeThing(thingComponent: FormGroup) {
        var things = <FormArray>this.formThings.controls['things'];

        var thingIndex = things.controls.indexOf(thingComponent);

        if (thingIndex !== -1) {
            things.removeAt(thingIndex);
        }
    }
}