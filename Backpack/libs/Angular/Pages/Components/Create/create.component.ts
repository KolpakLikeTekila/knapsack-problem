import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Router} from "@angular/router";
import {IRucksack} from "../../../Interfaces/IRucksack";
import {Global} from "../../../Shared/global";

@Component({
    templateUrl: Global.pathToView('create', 'create')
})

export class CreateComponent implements OnInit {
    Rucksack: IRucksack[];
    formRucksack: FormGroup;

    constructor(private formBuilder: FormBuilder, private http: Http, private router: Router) {
    }

    goMain() {
        this.router.navigate(['']);
    }

    ngOnInit(): void {
        this.formRucksack = this.formBuilder.group({
            capacity: [0],
            things: this.formBuilder.array([])
        });
    }

    post(url: string, model: any): Observable<any> {
        let body = JSON.stringify(model);
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.post(url, body, options);
    }

    createRucksack(formData: any): void {
        this.post("create", formData._value);
        this.goMain();
        
    }

    cansel() {
        this.goMain();
    }
}