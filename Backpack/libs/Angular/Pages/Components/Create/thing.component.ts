﻿import {Global} from "../../../Shared/global"; 
import {Component, EventEmitter, Input, Output} from "@angular/core"; 
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'thing',
    templateUrl: Global.pathToView('thing', 'create')
})

export class ThingComponent {
    @Input() formThing: FormGroup;
    @Output() removeThing = new EventEmitter<FormGroup>();

    remove() {
        this.removeThing.emit(this.formThing)
    }
}