"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var global_1 = require("../../Shared/global");
var thing_component_1 = require("./thing.component");
var forms_1 = require("@angular/forms");
var core_1 = require("@angular/core");
var ThingsComponent = (function () {
    function ThingsComponent(formBuilder) {
        this.formBuilder = formBuilder;
        this.things = [];
    }
    ThingsComponent.prototype.ngOnInit = function () {
        this.formThings.addControl('things', this.formBuilder.array([this.initThing()]));
    };
    ThingsComponent.prototype.initThing = function () {
        var thingGroup = this.formBuilder.group({
            Weight: [0],
            Name: [''],
            Cost: [0]
        });
        return thingGroup;
    };
    ThingsComponent.prototype.addThing = function () {
        var things = this.formThings.controls['things'];
        var thingGroup = this.formBuilder.group({
            Weight: [0],
            Name: [''],
            Cost: [0]
        });
        things.push(thingGroup);
    };
    ThingsComponent.prototype.removeThing = function (thingComponent) {
        // var thing = this.things.find((component) => component instanceof ThingComponent);
        var things = this.formThings.controls['things'];
        var thingIndex = things.controls.indexOf(thingComponent);
        if (thingIndex !== -1) {
            things.removeAt(thingIndex);
            //  this.things.splice(thingIndex, 1);
        }
    };
    return ThingsComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", forms_1.FormGroup)
], ThingsComponent.prototype, "formThings", void 0);
ThingsComponent = __decorate([
    core_1.Component({
        selector: 'things',
        templateUrl: global_1.Global.pathToView('things'),
        entryComponents: [thing_component_1.ThingComponent]
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder])
], ThingsComponent);
exports.ThingsComponent = ThingsComponent;
//# sourceMappingURL=things.component.js.map