"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
require("rxjs/add/operator/do");
require("rxjs/add/operator/catch");
var global_1 = require("../../../Shared/global");
var task_component_1 = require("./task.component");
var core_1 = require("@angular/core");
var TasksComponent = (function () {
    function TasksComponent(componentFactoryResolver, http) {
        this.componentFactoryResolver = componentFactoryResolver;
        this.http = http;
    }
    TasksComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.interval = setInterval(function () {
            _this.getTasks();
        }, 1000);
    };
    TasksComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.interval);
    };
    TasksComponent.prototype.getTasks = function () {
        var _this = this;
        this.get().subscribe(function (tasks) {
            _this.loadList(tasks);
        });
    };
    TasksComponent.prototype.get = function () {
        return this.http.get('list').map(function (response) { return response.json(); });
    };
    TasksComponent.prototype.loadList = function (tasks) {
        this.tasksContainer.clear();
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(task_component_1.TaskComponent);
        for (var _i = 0, tasks_1 = tasks; _i < tasks_1.length; _i++) {
            var task = tasks_1[_i];
            var dynamicComponent = this.tasksContainer.createComponent(componentFactory).instance;
            dynamicComponent.task = task;
        }
    };
    return TasksComponent;
}());
__decorate([
    core_1.ViewChild('tasks', { read: core_1.ViewContainerRef }),
    __metadata("design:type", core_1.ViewContainerRef)
], TasksComponent.prototype, "tasksContainer", void 0);
TasksComponent = __decorate([
    core_1.Component({
        selector: 'tasks',
        templateUrl: global_1.Global.pathToView('tasks', 'main'),
        entryComponents: [task_component_1.TaskComponent]
    }),
    __metadata("design:paramtypes", [core_1.ComponentFactoryResolver, http_1.Http])
], TasksComponent);
exports.TasksComponent = TasksComponent;
//# sourceMappingURL=tasks.component.js.map