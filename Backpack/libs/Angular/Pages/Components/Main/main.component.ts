﻿import {Component} from '@angular/core';
import {Global} from '../../../Shared/global';

@Component({
    templateUrl: Global.pathToView('main','main'),
})

export class MainComponent {
}