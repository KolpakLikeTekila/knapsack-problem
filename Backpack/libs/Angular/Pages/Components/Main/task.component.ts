﻿import {Component, Input} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Global} from "../../../Shared/global";
import {ITask} from "../../../Interfaces/ITask";

@Component({
    templateUrl: Global.pathToView('task','main')
})
export class TaskComponent {
    @Input() task: ITask;

    constructor( private http: Http) {
    }

    private post(url: string, model: any): Observable<any> {
        let body = JSON.stringify(model);
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.post(url, body, options);
    }

    removeTask(id: number): void {
        this.post("remove", id);
    }
}