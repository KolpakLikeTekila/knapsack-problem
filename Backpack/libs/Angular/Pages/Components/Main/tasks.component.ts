﻿import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch'
import {Global} from "../../../Shared/global";
import {ITask} from "../../../Interfaces/ITask";
import {TaskComponent} from "./task.component";
import {
    AfterViewInit, Component, ComponentFactoryResolver, OnDestroy, ViewChild,
    ViewContainerRef
} from "@angular/core";

@Component({
    selector: 'tasks',
    templateUrl: Global.pathToView('tasks', 'main'),
    entryComponents: [TaskComponent]
})

export class TasksComponent implements AfterViewInit, OnDestroy {

    private interval: any;
    @ViewChild('tasks', {read: ViewContainerRef}) tasksContainer: ViewContainerRef;

    constructor(private componentFactoryResolver: ComponentFactoryResolver, private http: Http) {
    }

    ngAfterViewInit() {
        this.interval = setInterval(() => {
            this.getTasks();
        }, 1000);
    }

    ngOnDestroy() {
        clearInterval(this.interval);
    }

    private getTasks(): void{
        this.get().subscribe(tasks => {
            this.loadList(tasks)
        });
    }

    private get(): Observable<any> {
        return this.http.get('list').map((response: Response) => <any>response.json());
    }

    private loadList(tasks:ITask[]) {
        this.tasksContainer.clear();
        var componentFactory = this.componentFactoryResolver.resolveComponentFactory(TaskComponent);

        for (let task of tasks) {
            let dynamicComponent = this.tasksContainer.createComponent(componentFactory).instance;
            dynamicComponent.task = task;
        }
    }
}