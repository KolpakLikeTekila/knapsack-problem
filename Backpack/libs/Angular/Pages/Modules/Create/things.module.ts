﻿import {NgModule} from '@angular/core';
import {ThingsComponent} from "../../Components/Create/things.component";
import {ThingComponent} from "../../Components/Create/thing.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule],
    declarations: [ThingsComponent, ThingComponent],
    bootstrap: [ThingsComponent]
})

export class ThingsModule {
}