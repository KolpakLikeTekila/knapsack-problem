﻿import {NgModule} from '@angular/core';
import {ThingComponent} from "../../Components/Create/thing.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [CommonModule],
    declarations: [ThingComponent],
    bootstrap: [ThingComponent]
})

export class ThingModule {
}