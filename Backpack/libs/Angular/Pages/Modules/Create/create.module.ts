﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms'; 
import {CreateComponent} from "../../Components/Create/create.component"; 
import {ThingsComponent} from "../../Components/Create/things.component";

@NgModule({
    imports: [BrowserModule, FormsModule],
    declarations: [CreateComponent, ThingsComponent],
    bootstrap: [CreateComponent]
})

export class CreateModule {
}