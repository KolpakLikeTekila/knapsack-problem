﻿import {NgModule} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {TaskComponent} from "../../Components/Main/task.component";
import {TasksComponent} from '../../Components/Main/tasks.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [TasksComponent, TaskComponent],
    bootstrap: [TasksComponent],
    entryComponents: [TaskComponent],
})

export class TasksModule {
}