﻿import {NgModule} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {MainComponent} from "../../Components/Main/main.component";
import {TasksComponent} from "../../Components/Main/tasks.component";
import {TaskComponent} from "../../Components/Main/task.component";

@NgModule({
    imports: [BrowserModule],
    declarations: [MainComponent, TasksComponent,TaskComponent],
    bootstrap: [MainComponent]
})

export class MainModule {
}