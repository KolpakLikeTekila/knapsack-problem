﻿import {IThing} from "./IThing";

export interface IRucksack {
    Capacity: string,
    Things: IThing[]
}