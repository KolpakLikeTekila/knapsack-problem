﻿export interface ITask {
    Name: string,
    Capacity: number,
    Cost: number,
    Percent: string,
    Status:boolean,
    Id: number
}