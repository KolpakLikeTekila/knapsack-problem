import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {routing} from './app.routing';
import {AppComponent} from "./app.component";
import {NotFoundComponent} from "./Pages/Components/not-found.component";
import {CreateComponent} from "./Pages/Components/Create/create.component";
import {ThingsComponent} from "./Pages/Components/Create/things.component";
import {ThingComponent} from "./Pages/Components/Create/thing.component";
import {MainComponent} from "./Pages/Components/Main/main.component";
import {TasksComponent} from "./Pages/Components/Main/tasks.component";
import {TaskComponent} from "./Pages/Components/Main/task.component";
import {CommonModule} from "@angular/common";

@NgModule({
    imports: [BrowserModule, ReactiveFormsModule, HttpModule, routing, CommonModule],
    declarations: [AppComponent, MainComponent, CreateComponent, NotFoundComponent, ThingsComponent, ThingComponent,TasksComponent,TaskComponent],
    bootstrap: [AppComponent]
})

export class AppModule {
}
