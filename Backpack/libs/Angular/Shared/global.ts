export class Global {

    public static pathToView(filename: string, dirName?: string): string {
        var pathToView = 'libs/angular/pages/views/';
        if (dirName) {
            pathToView+= dirName + '/';
        }
        return pathToView + filename + '.component.html';
    }
}