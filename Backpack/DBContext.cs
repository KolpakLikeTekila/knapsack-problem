﻿using System.Data.Entity;
using Backpack.Entity;

namespace Backpack
{
    public class DBContext : DbContext
    {
        public DBContext() : base("BackpackDB")
        {
        }
        public DbSet<Thing> Things { get; set; }
        public DbSet<Rucksack> Rucksacks { get; set; }
        public DbSet<TaskRusksack> TaskRusksacks { get; set; }
    }
}