﻿using System.Collections.Generic;
using System.Web.Mvc;
using Backpack.DomainServices;
using Backpack.DomainServices.Interfaces;
using Backpack.Dto;
using Backpack.Entity;
using Backpack.Models.Process;
using Backpack.ViewModels;
using Backpack.ViewModels.Interfaces;

namespace Backpack.Controllers
{
    public class RucksacksController : Controller
    {
        public IRucksackViewModel RucksackViewModel { get; set; }
        public IRucksackDomainService RucksackDomainService { get; set; }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult List()
        {
            var list = RucksackViewModel.List();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Detail(long? id)
        {
            if (id == null)
                return null;

            var rucksack = RucksackViewModel.Detail((long) id);
            return Json(rucksack, JsonRequestBehavior.AllowGet);
        }

        public void Remove(long? id)
        {
            if (id == null)
                return;
            Working.RemoveProcess((long) id);
            RucksackDomainService.Remove((long) id);
        }

        [HttpPost]
        public void Create(int capacity, List<Thing> things)
        {
            var rucksack = new Rucksack()
            {
                Capacity = capacity,
                Things = things,
                TaskRusksack = new TaskRusksack()
            };
            RucksackDomainService.Save(rucksack);
            Working.CreateProcess(rucksack);
        }
    }
}