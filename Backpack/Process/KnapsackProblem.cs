﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Backpack.DomainServices;
using Backpack.Dto;
using Backpack.Entity;

namespace Backpack.Models.Process
{
    public class KnapsackProblem
    {
        private readonly Stopwatch _stopWatch = new Stopwatch();

        public void Start(Rucksack rucksack, CancellationTokenSource token)
        {
            var task = rucksack.TaskRusksack;
            var things = new List<Thing>();
            var thingSolutions = new List<Thing>();
            using (var thingDS = new ThingDomainService())
            {
                var thingQueryable = thingDS.GetAll().Where(thing => thing.RucksackId == task.Rucksack.Id)
                    .OrderBy(thing => thing.Id);
                things = thingQueryable.ToList();
                thingSolutions = thingQueryable.Where(thing => thing.Take).ToList();
            }
            thingSolutions = new List<Thing>(thingSolutions);
            var cost = thingSolutions.Sum(thing => thing.Cost);
            var knapsackProblemDto = new KnapsackProblemDto
            {
                Token = token.Token,
                Id = task.Id,
                Time = task.Time,
                DeadIndexList = task.Combinat?.Split(',').Select(long.Parse).ToList() ?? new List<long>(),
                ThingOldSolutions = new List<Thing>(thingSolutions),
                ThingSolutions = thingSolutions,
                Things = new List<Thing>(things),
                Process = task.Process,
                Capacity = rucksack.Capacity,
                Cost = cost
            };

            var deadIndexList = knapsackProblemDto.DeadIndexList;
            things = things.Where(thing => !deadIndexList.Contains(thing.Id)).ToList();

            _stopWatch.Start();
            if (CheckSet(knapsackProblemDto, things) || things.Min(thing => thing.Weight) > rucksack.Capacity)
            {
                SaveTask(knapsackProblemDto, deadIndexList, true);
            }
            else
            {
                Sequence(knapsackProblemDto, things, deadIndexList);
                _stopWatch.Stop();
            }
        }

        private void Sequence(KnapsackProblemDto knapsackProblemDto, IList<Thing> things, IList<long> deadIndexList)
        {
            do
            {
                MakeAllSets(knapsackProblemDto, things, deadIndexList);
                if (knapsackProblemDto.Token.IsCancellationRequested)
                    return;

                RollbackIterList(things, deadIndexList);
            } while (deadIndexList.Count != 0);
            SaveTask(knapsackProblemDto, deadIndexList, true);
        }

        private void RollbackIterList(IList<Thing> thinglist, IList<long> deadIndexList)
        {
            if (!deadIndexList.Any())
                return;

            var thing = thinglist.Last(th => th.Id < deadIndexList.Last());
            var thingIndex = thinglist.IndexOf(thing);
            thinglist.Insert(thingIndex, thing);
            deadIndexList.RemoveAt(deadIndexList.Count - 1);
        }


        private void SaveTask(KnapsackProblemDto knapsackProblemDto, IList<long> deadIndexList, bool status = false)
        {
            _stopWatch.Stop();
            knapsackProblemDto.Time += _stopWatch.ElapsedMilliseconds;

            var thingOldSolutions = knapsackProblemDto.ThingOldSolutions
                .Except(knapsackProblemDto.ThingSolutions)
                .ToArray();

            foreach (var th in thingOldSolutions)
                th.Take = false;

            var thingSolutions = knapsackProblemDto.ThingSolutions.ToArray();
            foreach (var th in knapsackProblemDto.ThingSolutions)
                th.Take = true;

            using (var thingDS = new ThingDomainService())
            {
                thingDS.Edit(thingOldSolutions);
                thingDS.Edit(thingSolutions);
            }

            using (var taskDS = new TaskRusksackDomainService())
            {
                var task = taskDS.Get(knapsackProblemDto.Id);
                task.Status = status;
                task.Combinat = string.Join(",", deadIndexList);
                task.Process = knapsackProblemDto.Process;
                task.Time = knapsackProblemDto.Time;
                taskDS.Edit(task);
            }

            _stopWatch.Start();
        }

        private void MakeAllSets(KnapsackProblemDto knapsackProblemDto, IList<Thing> things, IList<long> deadIndexList)
        {
            if (knapsackProblemDto.Token.IsCancellationRequested)
                return;
            if (things.Any())
            {
                knapsackProblemDto.Process++;
                CheckSet(knapsackProblemDto, things);
            }
            foreach (var thing in things)
            {
                var newSet = things.Where(th => th.Id != thing.Id).ToList();
                deadIndexList.Add(thing.Id);
                SaveTask(knapsackProblemDto, deadIndexList);
                MakeAllSets(knapsackProblemDto, newSet, deadIndexList);
                deadIndexList.RemoveAt(deadIndexList.Count - 1);
            }
        }

        private bool CheckSet(KnapsackProblemDto knapsackProblemDto, IList<Thing> things)
        {
            var cost = things.Sum(thing => thing.Cost);
            var weight = things.Sum(thing => thing.Weight);

            if (weight <= knapsackProblemDto.Capacity && cost > knapsackProblemDto.Cost)
            {
                knapsackProblemDto.ThingSolutions = things;
                knapsackProblemDto.Cost = cost;
                return true;
            }
            return false;
        }
    }
}