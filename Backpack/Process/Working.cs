﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Backpack.DomainServices;
using Backpack.Entity;

namespace Backpack.Models.Process
{
    public class Working
    {
        private static readonly Dictionary<long, CancellationTokenSource> DicToken =
            new Dictionary<long, CancellationTokenSource>();

        public static void Start()
        {
            var rucksacks = new List<Rucksack>();
            using (var rucksackDS = new RucksackDomainService())
            {
                rucksacks = rucksackDS.GetAll().Where(rucksack => !rucksack.TaskRusksack.Status).ToList();
            }
            Task.Run(() =>
            {
                Parallel.ForEach(rucksacks, (rucksack, state) =>
                {
                    var token = new CancellationTokenSource();
                    DicToken.Add(rucksack.Id, token);
                    new KnapsackProblem().Start(rucksack, token);
                });
            });
        }

        public static void CreateProcess(Rucksack rucksack)
        {
            var token = new CancellationTokenSource();
            DicToken.Add(rucksack.Id, token);
            Task.Run(() => new KnapsackProblem().Start(rucksack, token), token.Token);
        }

        public static void RemoveProcess(long id)
        {
            var token = new CancellationTokenSource();
            if (DicToken.TryGetValue(id, out token))
            {
                token.Cancel();
                DicToken.Remove(id);
            }
        }
    }
}