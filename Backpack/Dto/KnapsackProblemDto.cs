﻿using System.Collections.Generic;
using System.Threading;
using Backpack.Entity;

namespace Backpack.Dto
{
    // <summary> Дто решения задачи о ранце </summary>
    public sealed class KnapsackProblemDto
    {
        /// <summary> Идентификатор решения задачи о рюкзаке</summary>
        public long Id { get; set; }

        /// <summary> Лучшее решение </summary>
        public IList<Thing> ThingSolutions { get; set; }

        /// <summary> Старое лучшее решение </summary>
        public IList<Thing> ThingOldSolutions { get; set; }

        /// <summary> Вещи </summary>
        public IList<Thing> Things { get; set; }

        /// <summary> Список идентификаторов пройденных комбинаций вещей</summary>
        public IList<long> DeadIndexList { get; set; }

        /// <summary> Время выполнения </summary>
        public long Time { get; set; }

        /// <summary> Выполненное количество итераций </summary>
        public long Process { get; set; }

        /// <summary> Токен отмены</summary>
        public CancellationToken Token { get; set; }

        /// <summary> Вместимость рюкзака </summary>
        public int Capacity { get; set; }

        /// <summary> Ценность </summary>
        public int Cost { get; set; }
    }
}