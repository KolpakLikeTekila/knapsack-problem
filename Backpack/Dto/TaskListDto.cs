﻿namespace Backpack.Dto
{
    /// <summary> Дто списка задач </summary>
    public sealed class TaskListDto
    {
        /// <summary> Идентификатор ранца </summary>
        public long Id { get; set; }

        /// <summary> Номер задачи </summary>
        public string Name { get; set; }

        /// <summary> Вместимость рюкзака </summary>
        public int Capacity { get; set; }

        /// <summary> Ценность </summary>
        public int Cost { get; set; }

        /// <summary> Процент выполнения </summary>
        public string Percent { get; set; }

        /// <summary> Статус </summary>
        public bool Status { get; set; }
    }
}