﻿namespace Backpack.Dto
{
    /// <summary> Дто вещи </summary>
    public sealed class ThingDto
    {
        /// <summary> Название вещи </summary>
        public string Name { get; set; }

        /// <summary> Ценность вещи </summary>
        public int Cost { get; set; }

        /// <summary> Вес вещи </summary>
        public int Weight { get; set; }
    }
}