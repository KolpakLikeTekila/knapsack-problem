﻿using System.Collections.Generic;

namespace Backpack.Dto
{
    /// <summary> Дто задачи </summary>
    public sealed class TaskDto
    {
        /// <summary> Номер задачи </summary>
        public string Name { get; set; }

        /// <summary> Вместимость рюкзака </summary>
        public int Capacity { get; set; }

        /// <summary> Максимальна ценность </summary>
        public int Cost { get; set; }

        /// <summary> Статус </summary>
        public bool Status { get; set; }

        /// <summary> Время выполнения </summary>
        public string Time { get; set; }

        /// <summary> Вещи </summary>
        public IList<ThingDto> Things { get; set; }

        /// <summary> Вещи, которые  являются решением </summary>
        public IList<ThingDto> ThingSolutions { get; set; }
    }
}