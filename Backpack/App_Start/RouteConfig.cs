﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Backpack
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                null,
                "list",
                new {controller = "Rucksacks", action = "List"}
            );
            routes.MapRoute(
                null,
                "detail",
                new {controller = "Rucksacks", action = "Detail"}
            );
            routes.MapRoute(
                null,
                "create",
                new {controller = "Rucksacks", action = "Create"}
            );
            routes.MapRoute(
                null,
                "remove",
                new {controller = "Rucksacks", action = "Remove"}
            );


            routes.MapRoute(
                null,
                "{*url}",
                new {controller = "Rucksacks", action = "Index"}
            );


            /*        routes.MapRoute(
                         name: "Default",
                         url: "{controller}/{action}/{id}",
                         defaults: new { controller = "Rucksacks", action = "Index", id = UrlParameter.Optional }
                     );*/
        }
    }
}