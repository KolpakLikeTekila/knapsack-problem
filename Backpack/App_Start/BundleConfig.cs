﻿using System.Web.Optimization;
using System.Web.Optimization.React;

namespace Backpack
{
    public class BundleConfig
    {
        //Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство сборки на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css"));

            bundles.Add(new BabelBundle("~/bundles/main").Include(
                "~/libs/ReactJS/Pages/*.jsx",
                "~/libs/ReactJS/Pages/Create/Items/*.jsx",
                "~/libs/ReactJS/Pages/Create/*.jsx",
                "~/libs/ReactJS/Pages/Detail/Items/*.jsx",
                "~/libs/ReactJS/Pages/Detail/*.jsx",
                "~/libs/ReactJS/Pages/Main/Items/*.jsx",
                "~/libs/ReactJS/Pages/Main/*.jsx",
                "~/libs/ReactJS/app.jsx"
            ));
        }
    }
}