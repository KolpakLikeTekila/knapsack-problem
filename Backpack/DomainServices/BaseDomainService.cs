﻿using System;
using System.Data.Entity;
using System.Linq;
using Backpack.DomainServices.Interfaces;
using Backpack.Entity.Interface;

namespace Backpack.DomainServices
{
    public abstract class BaseDomainService<TEntity> : IBaseDomainService<TEntity> where TEntity : class, IHasId
    {
        protected DBContext DbContext { get; private set; } = new DBContext();

        private IDbSet<TEntity> DbSet => DbContext.Set<TEntity>();
        
        public virtual IQueryable<TEntity> GetAll()
        {
            return DbContext.Set<TEntity>();
        }

        //TODO придумать чтобы проходило по всем связям
        public virtual void Save(TEntity entity)
        {
            Save(new[] {entity});
        }

        public virtual void Save(params TEntity[] entities)
        {
            foreach (var entity in entities)
                DbSet.Add(entity);
            DbContext.SaveChanges();
        }

        public virtual TEntity Get(long id)
        {
            return DbSet.Find(id);
        }

        public virtual void Remove(long id)
        {
            var en = Get(id);
            if (en == null)
                return;
            DbSet.Remove(en);
            DbContext.SaveChanges();
        }

        public void Edit(TEntity entity)
        {
            Edit(new[] {entity});
        }

        protected abstract void EditParams(TEntity entity);

        public void Edit(params TEntity[] entities)
        {
            foreach (var entity in entities)
            {
                EditParams(entity);
            }
            DbContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposing)
                return;
            if (DbContext == null)
                return;
            DbContext.Dispose();
            DbContext = null;
        }
    }
}