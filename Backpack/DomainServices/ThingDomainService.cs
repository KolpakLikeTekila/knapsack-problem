﻿using System.Linq;
using Backpack.DomainServices.Interfaces;
using Backpack.Entity;

namespace Backpack.DomainServices
{
    public class ThingDomainService : BaseDomainService<Thing>, IThingDomainService
    {
        protected override void EditParams(Thing entity)
        {
            var thing = Get(entity.Id);
            if (thing == null) return;
            thing.Cost = entity.Cost;
            thing.Name = entity.Name;
            thing.Weight = entity.Weight;
            thing.Take = entity.Take;
        }
    }
}