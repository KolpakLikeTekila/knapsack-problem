﻿using System.Data.Entity;
using System.Linq;
using System.Xml;
using Backpack.DomainServices.Interfaces;
using Backpack.Entity;
using Backpack.Models.Process;

namespace Backpack.DomainServices
{
    public sealed class RucksackDomainService : BaseDomainService<Rucksack>, IRucksackDomainService
    {
        public ITaskRusksackDomainService TaskRusksackDomainService { get; set; }
        
        public override void Save(Rucksack entity)
        {
            TaskRusksackDomainService.NextName(entity.TaskRusksack);
            base.Save(entity);
        }

        public override Rucksack Get(long id)
        {
            return GetAll().Include(rucksack => rucksack.TaskRusksack).FirstOrDefault(rucksack => rucksack.Id == id);
        }

// без дублирование происходит ошибка в DbContext.SaveChanges()         
        public override void Remove(long id)
        {
            var en = Get(id);
            if (en == null)
                return;
            base.Remove(id);
        }

        public override IQueryable<Rucksack> GetAll()
        {
            return DbContext.Set<Rucksack>().Include(rucksack => rucksack.TaskRusksack);
        }

        protected override void EditParams(Rucksack entity)
        {
            var rucksack = Get(entity.Id);
            if (rucksack == null) return;
            rucksack.Capacity = entity.Capacity;
        }
    }
}