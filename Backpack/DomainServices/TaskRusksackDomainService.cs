﻿using System.Linq;
using Backpack.DomainServices.Interfaces;
using Backpack.Entity;

namespace Backpack.DomainServices
{
    public class TaskRusksackDomainService :  BaseDomainService<TaskRusksack>, ITaskRusksackDomainService
    {
        public override void Save(TaskRusksack entity)
        {
            NextName(entity);
            base.Save(entity);
        }

        public void NextName(TaskRusksack entity)
        {
            int? name = GetAll().OrderByDescending(task => task.Name).Select(task => task.Name).FirstOrDefault();
            if (name == null)
                name = 0;
            name++;
            entity.Name = (int) name;
        }

        protected override void EditParams(TaskRusksack entity)
        {
            var task = Get(entity.Id);
            if (task == null) return;
            task.Status = entity.Status;
            task.Process = entity.Process;
            task.Time = entity.Time;
            task.Combinat = entity.Combinat;
        }
    }
}