﻿using Backpack.Entity;

namespace Backpack.DomainServices.Interfaces
{
    public interface ITaskRusksackDomainService: IBaseDomainService<TaskRusksack>
    {
        void NextName(TaskRusksack entity);
    }
}