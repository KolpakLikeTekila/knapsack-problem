﻿using Backpack.Entity;

namespace Backpack.DomainServices.Interfaces
{
    public interface IRucksackDomainService: IBaseDomainService<Rucksack>
    {
    }
}