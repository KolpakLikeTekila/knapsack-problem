﻿using Backpack.Entity;

namespace Backpack.DomainServices.Interfaces
{
    public interface IThingDomainService: IBaseDomainService<Thing>
    {
    }
}