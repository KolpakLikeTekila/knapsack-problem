﻿namespace Backpack.Entity.Interface
{
    public interface IHasId
    {
        long Id { get; set; }
    }
}