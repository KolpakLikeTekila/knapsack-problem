﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Backpack.Entity.Interface;

namespace Backpack.Entity
{
    /// <summary> Рюкзак </summary>
    public class Rucksack : IHasId
    {
        /// <summary> Вместимость рюкзака </summary>
        [Display(Name = "ВМЕСТИМОСТЬ РЮКЗАКА")]
        [Range(0, int.MaxValue)]
        [Required(ErrorMessage = "Пожалуйста, введите вместимость рюкзакa ")]
        public int Capacity { get; set; }

        /// <summary> Вещи в рюкзаке </summary>
        public virtual ICollection<Thing> Things { get; set; } = new List<Thing>();

        /// <summary> Решение задачи о ранце </summary>
        public virtual TaskRusksack TaskRusksack { get; set; }

        /// <summary> Идентификатор </summary>
        public long Id { get; set; }
    }
}