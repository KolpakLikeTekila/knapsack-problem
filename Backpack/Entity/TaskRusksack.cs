﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Backpack.Entity.Interface;

namespace Backpack.Entity
{
    /// <summary> Решение задачи о рюкзаке </summary>
    public class TaskRusksack : IHasId
    {
        /// <summary> Номер задачи </summary>
        [Display(Name = "№ ЗАДАЧИ")]
        public int Name { get; set; }

        /// <summary> Статус выподнения </summary>
        [Display(Name = "СТАТУС")]
        public bool Status { get; set; }

        /// <summary> Выполненное количество итераций </summary>
        [Display(Name = "Количество итераций")]
        public long Process { get; set; } = 0;

        /// <summary> Время выполнения </summary>
        [Display(Name = "ВРЕМЯ ВЫПОЛНЕНИЯ")]
        public long Time { get; set; }

        /// <summary> Промежуточные шаги выполнения </summary>
        [ScaffoldColumn(false)]
        public string Combinat { get; set; }

        /// <summary> Рюкзак </summary>
        public virtual Rucksack Rucksack { get; set; }

        /// <summary> Идентификатор </summary>
        [Key]
        [ForeignKey("Rucksack")]
        public long Id { get; set; }
    }
}