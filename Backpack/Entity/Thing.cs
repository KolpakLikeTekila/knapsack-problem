﻿using System.ComponentModel.DataAnnotations;
using Backpack.Entity.Interface;

namespace Backpack.Entity
{
    /// <summary> Вещь </summary>
    public class Thing : IHasId
    {
        /// <summary> Идентификатор </summary>
        [ScaffoldColumn(false)]
        public bool Take { get; set; } = false;

        /// <summary> Название вещи </summary>
        [Display(Name = "ПРЕДМЕТ")]
        [Required(ErrorMessage = "Пожалуйста, введите предмета ")]
        public string Name { get; set; }

        /// <summary> Ценность вещи </summary>
        [Display(Name = "ЦЕННОСТЬ")]
        [Range(0, int.MaxValue)]
        [Required(ErrorMessage = "Цена должна быть положительна")]
        public int Cost { get; set; }

        /// <summary> Вес вещи </summary>
        [Display(Name = "ВЕС")]
        [Range(0, int.MaxValue)]
        [Required(ErrorMessage = "Вес должен быть положительным ")]
        public int Weight { get; set; }

        /// <summary> Идентификатор рюкзака </summary>
        public long RucksackId { get; set; }

        /// <summary> Рюкзак </summary>
        public virtual Rucksack Rucksack { get; set; }

        /// <summary> Идентификатор </summary>
        [ScaffoldColumn(false)]
        public long Id { get; set; }
    }
}